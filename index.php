<!DOCTYPE html>

<?php
include("Model.php");

$model = new Model();
$connection = $model->getConnection();



if(isset($_POST['submit']))
{
$link = mysqli_connect("localhost", "root", "", "icubetest");
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

$product_name = mysqli_real_escape_string($link, $_REQUEST['product_name']);
$cgid = mysqli_real_escape_string($link, $_REQUEST['cg_id']);
$cg_id = '';
if($cgid == "1") { $cg_id = "Retail";} else { $cg_id = "Wholesale"; }
$price = mysqli_real_escape_string($link, $_REQUEST['price']);
$isAnUpdate = mysqli_real_escape_string($link, $_REQUEST['update']);

// Check for duplicates
$checkProduct = $model->getData("SELECT * FROM products WHERE product_name LIKE '$product_name'");
$checkPrice =  $model->getData("SELECT * FROM price WHERE price LIKE '$price'");
$checkGroup =  $model->getData("SELECT * FROM customer_group WHERE customer_group LIKE '$cg_id'");
if ($checkProduct && $checkPrice && $checkGroup && $isAnUpdate == "No") {
	echo "<h3>Data yang anda masukkan sama persis dengan sebuah entri yang sudah ada. Data duplikat tidak diperbolehkan.</h3>";
} 
// Update Entri
else if ($checkProduct && $checkPrice && $checkGroup && $isAnUpdate == "Yes") {

$checkPrice = $checkPrice[0]['price'];

$updated_product_name = mysqli_real_escape_string($link, $_REQUEST['updated_product_name']);
$updated_cgid = mysqli_real_escape_string($link, $_REQUEST['updated_cg_id']);
$updated_cg_id = '';
if($updated_cgid == "1") { $cg_id = "Retail";} else { $updated_cg_id = "Wholesale"; }
$updated_price = mysqli_real_escape_string($link, $_REQUEST['updated_price']);

	$getPriceId = $model->getData("SELECT price_id FROM price WHERE price LIKE '$checkPrice'");
	$getPriceId = $getPriceId[0]['price_id'];
	$getProductId = $model->getData("SELECT product_id FROM price WHERE price LIKE '$checkPrice'");
	$getProductId = $getProductId[0]['product_id'];
	$getCGId = $model->getData("SELECT cg_id FROM price WHERE price LIKE '$checkPrice'");
	$getCGId = $getCGId[0]['cg_id'];

	$updateProductName = "UPDATE products SET product_name = '$updated_product_name' WHERE product_id = " . $getProductId;
	$updatePrice = "UPDATE price SET price = " .  $updated_price . " WHERE price_id = " . $getPriceId;
	$updateCG = "UPDATE price SET cg_id = '$updated_cgid' WHERE price_id = " . $getPriceId;

if(mysqli_query($link, $updateProductName) && mysqli_query($link, $updatePrice) && mysqli_query($link, $updateCG)){
    echo "<h3>Berhasil mengupdate entri.</h3>";
} else{
    echo "<h3>Gagal mengupdate entri $sql. " . mysqli_error($link) . "</h3>";
}
}

// Buat entri baru
else {
$insertProductName = "INSERT INTO products (product_name) VALUES ('$product_name')";
mysqli_query($link, $insertProductName);
$getProductId = $model->getData("SELECT product_id FROM products WHERE product_name LIKE '$product_name'");
$productIdValue = $getProductId[0]['product_id'];
$insertCustomerGroupName = "INSERT INTO customer_group (customer_group) VALUES ('$cgid')";
$getCustomerGroupId = $model->getData("select cg_id from customer_group WHERE customer_group = " . $cgid);
$insertPrice = "INSERT INTO price (product_id, cg_id, price) VALUES ('$productIdValue','$cgid','$price')";

if(mysqli_query($link, $insertProductName) && mysqli_query($link, $insertPrice)){
    echo "<h3>Berhasil menambahkan entri.</h3>";
} else{
    echo "<h3>Gagal menambahkan entri $sql. " . mysqli_error($link) . "</h3>";
}
}

}
?>



<head><title>Test SMK ICUBE by SIRCLO</title>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<link rel="stylesheet" href="css/style.css">

</head>
<body>
<form action="index.php" method="POST">
		<img src="img/ICUBEbySIRCLO.jpg" alt="icube" width=200/>
		<label for="product_name">Product Name</label>
		<input id="product_name" type="text" name="product_name" value="" />
		<label for="customer_group">Customer Group</label>
		<input id="r1" type="radio" name="cg_id" value="1" /> Retail
		<input id="r2" type="radio" name="cg_id" value="2"/> Wholesale
		<label for="price">Price</label>
		<input id="price" type="number" name="price" value="" />

		<div class="updateWrapper">
		<hr>
		<label for="price">isAnUpdate?</label>
		<input id="update"  name="update" value="No" />

		<label for="product_name">Updated Product Name</label>
		<input disabled id="updated_product_name" type="text" name="updated_product_name" value="" />
		<label for="customer_group">Updated Customer Group</label>
		<input disabled id="updated_r1" type="radio" name="updated_cg_id" value="1" /> Retail
		<input disabled id="updated_r2" type="radio" name="updated_cg_id" value="2"/> Wholesale
		<label for="price">Updated Price</label>
		<input disabled id="updated_price" type="number" name="updated_price" value="" />
	</div>

		<button type="submit" name="submit" value="submit" >Save</button>
</form>
<table>
	<thead>
		<tr><th>Product Name</th><th>Customer Group</th><th>Price</th><th></th></tr>
	</thead>
	<tbody>

		<?php $rows = $model->getData("select * from price ORDER BY price DESC");
		foreach ($rows as $row)
  		{ 
  			$priceID = $row['price_id'];
  			?>

	<tr>


	<?php $productId = $model->getData("select product_id from price WHERE price_id = " . $priceID); ?>
	<?php $productName = $model->getData("select product_name from products WHERE product_id = " . $productId[0]['product_id']); ?>
	<td><?php echo $productName[0]['product_name'] ?></td>


	<?php $customerGroupId = $model->getData("select cg_id from price WHERE price_id = " . $priceID); ?>
	<?php $customerGroupName = $model->getData("select customer_group from customer_group WHERE cg_id = " . $customerGroupId[0]['cg_id']); ?>
	<td><?php echo $customerGroupName[0]['customer_group'] ?></td>

	<td id="<?php echo $row['price_id']; ?>">$ <?php echo $row['price']; ?></td>

	<td><button class="edit" name="edit[]" onclick="addUpdate('<?php echo $row['price']; ?>','<?php echo $customerGroupId[0]['cg_id']
	; ?>','<?php echo $productName[0]['product_name']; ?>')"> Edit</button></td>
	</tr>


<?php   }   ?>

	</tbody>
</table>

</body>
</html>